#! /bin/bash 

LOCATION=$1 

function buildXmlInfoLine {
	filename=$(basename $1)
	echo "<photo><filename>$filename</filename><caption>$filename</caption><title>$2</title><metadata><width>$3</width><height>$4</height></metadata><tags>$5</tags></photo>" 
}

echo "<photos>"
ls -1 $LOCATION/*.jpg | while read photo
	do 
		filename=$(basename $photo)
		title=$(exiv2 -g Xmp.dc.title $photo | awk '{print $NF}')
		width=$(exiv2 $photo | grep "Image size" | cut -d ':' -f2 | cut -d ' ' -f2)    
		height=$(exiv2 $photo | grep "Image size" | cut -d ':' -f2 | cut -d ' ' -f4)    
		tags="<tag>$(exiv2 -g Xmp.dc.subject $photo | tr -s ' ' | cut -d ' ' -f4- | grep -o 'ap-.*' | sed  's/, /<\/tag><tag>/g')</tag>"
		buildXmlInfoLine $photo $title $width $height $tags
	done
echo "</photos>"
